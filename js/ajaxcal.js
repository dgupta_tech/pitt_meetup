
/*$(document).ready(function() {
  $('#content').load("calformat.html");
} */

function grabCalendar() {
  var xmlHttp = createXmlHttp();
  xmlHttp.onreadystatechange = function () {
    if(xmlHttp.readyState == 4) {
      var entryList = JSON.parse(xmlHttp.responseText);
      generateCalendar(entryList);
    }
  }
  var items = document.getElementsByName('activity');
  var params = ""
  for (var i = 0; i < items.length; i++) {
    if(items[i].checked) {
      params += "&";
      params += encodeParameter('activity', items[i].value);
    }
  }
  postParameters(xmlHttp, '/Calendar/calrequest', params);
}

function generateCalendar(list) {
  var text = ''
  var visitedlist = []
  if (!list || list.length == 0) {
    text += '<li>No entries.</li>';
  } else {
    for (var i = 0; i < list.length; i++) {
      text = list[i].loc + '\n' + list[i].start + '-' + list[i].end;
      if(list[i].date) {
        console.log('list day is ' + (list[i].date));
        myindex=list[i].date.indexOf(',');
        myindex--;
        console.log('val is ' + myindex);
        //document.getElementById('5').innertext = 'hi';
        var x = document.getElementById(myindex.toString())
        x.innerHTML='';
        var node = document.createTextNode(text);

        x.appendChild(node);
        visitedlist.append(myindex);
      }
    }
    for (var i = 0; i < 32; i++) {
      if (visitedlist.indexOf(i) == -1) {
        var x = document.getElementById(visitedlist[i].toString())
        x.innerHTML='';  
      }
      
    }
  }
  
}

function encodeParameter(name, value) {
  return encodeURIComponent(name) + "=" + encodeURIComponent(value);
}

function createXmlHttp() {
  var xmlhttp;
  if(window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
  } else {
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  return xmlhttp;
}

function postParameters(xmlHttp, target, parameters) {
  if(xmlHttp) {
    xmlHttp.open("POST", target, true);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.send(parameters);
  }
}