import webapp2
import os
import random
import time

from google.appengine.ext.webapp import template
from google.appengine.ext import ndb

class Event(ndb.Model) :
	name = ndb.TextProperty()
	loc = ndb.TextProperty()
	start = ndb.StringProperty()
	end = ndb.StringProperty()

def renderTemplate(templatename, templatevalues) :
	return template.render(templatename, templatevalues)
    
class EventPoster(webapp2.RequestHandler) :
	def get(self) :
		
		events = Event.query()
	
		template_values = {
			'events' : events
		}
  
		self.response.out.write(renderTemplate('event.html', template_values))
	
	def post(self) :
		
		temp = Event()
		temp.name = self.request.get('evt')
		temp.loc = self.request.get('loc')
		temp.start = self.request.get('start')
		temp.end = self.request.get('end')
		temp.put()
		time.sleep(1)
		events = Event.query()
		
		template_values = {
			'events' : events
		}
		self.response.out.write(renderTemplate('event.html', template_values))

application = webapp2.WSGIApplication([
	('/EventPoster', EventPoster),
], debug=True)


















