import logging
from google.appengine.api import users
from google.appengine.ext import ndb
from datetime import datetime
from datetime import date
import datetime
import calendar
import os
import cgi  
import webapp2
from google.appengine.ext.webapp import template

class Entry(ndb.Model):
    user_email = ndb.StringProperty()
    location = ndb.StringProperty()
    start_time = ndb.StringProperty()
    end_time = ndb.StringProperty()
    is_recurring = ndb.BooleanProperty()
    date = ndb.DateProperty()
    days_recurring = ndb.IntegerProperty()

class Activity(ndb.Model):
    activity = ndb.StringProperty()
    entries = ndb.StructuredProperty(Entry, repeated=True)

class User(ndb.Model):
  last_name = ndb.StringProperty()
  first_name = ndb.StringProperty()
  email = ndb.StringProperty()
  phone = ndb.StringProperty()
  age = ndb.StringProperty()
  major = ndb.StringProperty()
  gender = ndb.StringProperty()
  school_year = ndb.StringProperty()
  primary_contact = ndb.StringProperty()
  full_name = ndb.StringProperty()
  activities = ndb.StringProperty(repeated=True)

CALENDAR_HTML = """\
<html>
<head>
<title> Activity Calendar </title>
<h1 > Activity Calendar </h1>
<link rel="stylesheet" type="text/css" href="/stylesheets/stylesheet.css"/>
<link rel="stylesheet" type="text/css" href="/stylesheets/calendar.css"/>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script type="text/javascript" src="/js/first.js"></script>
<script type="text/javascript">
$(function () {
    $('[contenteditable=true]')
        .focus(function() {
            $(this).data("original", $(this).html());
        })
        .blur(function() {
//after user clicks off
            if ($(this).data("original") !== $(this).html() && $(this).html()!=='') {
//compare original contents with current(need to trim white spaces for comparison)
                $(this).html($(this).html()+ ' [deg62]');//add user signature after entered time
            }
        });
});


</script>
</head>
  <body>
    <div id="whole">
    <div id="leftside">
    <button onclick="show_calendar_update_window(this);">Add Your Availability</button> 
    <div id="add_availability"></div>
    </div>
    <div id="rightside">
    <div id="calendar"><table border="4" align="center">
      <tr>
         <td id="title" colspan="7" align="center">November 2014</td>
      </tr>
      <tr class="daysweek">
        <td align="center"> Sun</td>
        <td align="center"> Mon</td>
        <td align="center"> Tue</td>
        <td align="center"> Wed</td>
        <td align="center"> Thu</td>
        <td align="center"> Fri</td>
        <td align="center"> Sat</td>
      </tr>
        
       <tr class="days">
        <td >26</td>
        <td >27</td>
        <td >28</td>
        <td>29</td>
        <td>30</td>
        <td>31</td>
        <td>1</td>
      </tr>

       <tr class="editrow">
         <td class="edit" contenteditable="true" align="left" valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"><div style="min-height: 50px;"></div></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left" valign="top" ></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
      </tr>

     <tr class="days">
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        <td>6</td>
        <td>7</td>
        <td>8</td>
      </tr>
  
       <tr  class="editrow">
         <td class="edit" contenteditable="true" align="left" valign="top"> <div style="min-height: 50px;"></div></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left" valign="top" ></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
      </tr>

      <tr class="days">
        <td>9</td>
        <td>10</td>
        <td>11</td>
        <td>12</td>
        <td>13</td>
        <td>14</td>
        <td>15</td>
      </tr>

       <tr  class="editrow">
         <td class="edit" contenteditable="true" align="left" valign="top"> <div style="min-height: 50px;"></div></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left" valign="top" ></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
      </tr>

     <tr class="days">
        <td >16</td>
        <td >17</td>
        <td >18</td>
        <td >19</td>
        <td >20</td>
        <td >21</td>
        <td >22</td>
      </tr>
       
       <tr  class="editrow">
         <td class="edit" contenteditable="true" align="left" valign="top"> <div style="min-height: 50px;"></div></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left" valign="top" ></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
      </tr>

     <tr class="days">
        <td >23</td>
        <td>24</td>
        <td >25</td>
        <td >26</td>
        <td>27</td>
        <td >28</td>
        <td >29</td>
      </tr>
  
       <tr class="editrow">
         <td class="edit" contenteditable="true" align="left" valign="top"> <div style="min-height: 50px;"></div></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left" valign="top" ></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
         <td class="edit" contenteditable="true" align="left"  valign="top"></td>
      </tr>
    </table>
    </div>
    </div>
    </div>
    
   
  </body>
</html>
"""

def render_template(handler, templatename, templatevalues) :
  html = template.render(templatename, templatevalues)
  handler.response.out.write(html)

class Week:
    pass

class Val:
    pass

class Calendar(webapp2.RequestHandler):
    def get(self):
        #add user check here and redirect to / if no user
        now = datetime.datetime.now()
        month=now.strftime("%B")
        year=now.year
        ac_strings=[]
        days=[]
        vals=[]
        ids=[]
        isFirstWeek=1
        day=1
        activities=Activity.query(Activity.activity!='').fetch()
        for activity in activities:
         ac_strings.append(activity.activity)
        for row in range(0,5):
         column=0
         
         first_day=datetime.datetime(now.year,now.month,calendar.monthrange(now.year,now.month)[0])
         first_date=int(first_day.strftime("%d"))#casts first_day to int representing first date of the month
         if isFirstWeek==1:
          column=first_date
          isFirstWeek=0
         for col in range(column,7):
          if day>last_date:
           day=1
          val=Val()
          days.append(str(day))
          val.id=(str(day))
          val.val=''
          vals.append(val)
         week=Week()
         week.days=days
         week.vals=vals
         weeks.append(week)
         day=day+1
         last_day=datetime.datetime(now.year,now.month,calendar.monthrange(now.year,now.month)[1])
         last_date=int(last_day(strftime("%d"))#casts last_day to int representing last day of the month
        templatevalues = {
         'month': month,          
         'year' : year,
         'weeks': weeks,
         'activities': ac_strings,
        }
       render_template(self, 'Calendar.html', templatevalues)

    def post(self):
        entry = Entry()
        entry.days_recurring = 0;
        activity = Activity.query(Activity.activity == self.request.get('activity')).fetch()
        #if activity:
        user = users.get_current_user()
        fetched_user = User.query(User.email==user.email()).fetch()
        entry.user_email = user.email()
        entry.location = self.request.get('location')
        entry.start_time = self.request.get('start_time')
        entry.end_time = self.request.get('end_time')
        if self.request.get('recurring') == 'Yes':
            for day in self.request.get('weekday', allow_multiple=True):
                entry.days_recurring = entry.days_recurring | int(day)
        else :        
            entry.date = datetime.strptime(self.request.get('date'), '%m/%d/%Y')
        
        if activity:
            activity[0].entries.append(entry)
            activity[0].put()
            if fetched_user:
                if activity not in fetched_user[0].activities:
                    fetched_user[0].activities.append(activity[0].activity)
                    fetched_user[0].put()

        else:
            activity = Activity()
            activity.activity=self.request.get('activity')
            activity.entries.append(entry)
            activity.put()
            if fetched_user:
                if activity not in fetched_user[0].activities:
                    fetched_user[0].activities.append(activity.activity)
                    fetched_user[0].put()
        #else:
            #so that we can check for bad activity names
            #self.response.out.write('<html><script>alert("The selected activity does not exist! Please refer to our activity submission form to add it to our list");</script><body></body></html>')

        self.get()
        #then for the other thing. the filter thing. that's a function where you're gonna get a list of entries and return the filtered entries
        

class GetCalendar(webapp2.RequestHandler):
  def get(self) :
    self.post()
    
  def post(self) :
    entries = list()
    id = self.request.get('activity', allow_multiple=True)
    for act in id :
      
      q = Activity.query(Activity.activity == act).fetch()
      if q :
        for activity in q:
          
          for entry in activity.entries :
            entries.append(entry)
    values = {
      'entries' : entries
    }
    render_template(self, 'calendar.json', values)


application = webapp2.WSGIApplication([
    ('/Calendar', Calendar),
    ('/Calendar/calrequest', GetCalendar),
], debug=True) 