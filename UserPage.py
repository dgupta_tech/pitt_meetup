from google.appengine.api import users
from google.appengine.ext import ndb
import os
import cgi  
import webapp2
from google.appengine.ext.webapp import template

class Entry(ndb.Model):
    user_email = ndb.StringProperty()
    location = ndb.StringProperty()
    start_time = ndb.StringProperty()
    end_time = ndb.StringProperty()
    is_recurring = ndb.BooleanProperty()
    date = ndb.StringProperty()
    days_recurring = ndb.IntegerProperty()

class Activity(ndb.Model):
    activity = ndb.StringProperty()
    entries = ndb.StructuredProperty(Entry, repeated=True)

class User(ndb.Model):
  last_name = ndb.StringProperty()
  first_name = ndb.StringProperty()
  email = ndb.StringProperty()
  phone = ndb.StringProperty()
  age = ndb.StringProperty()
  major = ndb.StringProperty()
  gender = ndb.StringProperty()
  school_year = ndb.StringProperty()
  primary_contact = ndb.StringProperty()
  full_name = ndb.StringProperty()
  activities = ndb.StringProperty(repeated=True)

CANCEL_HTML="""
<html>
<head>
<script type="text/javascript">
$(document).ready(function(){
    $('html,body').animate({
          scrollTop: 460
        }, 600);
});
</script>
</head>
<body>
<form action="/UserPage" method="get">
  <input class="edit_buttons" type="submit" value="Cancel"/>
</form>
</body>
</html>
"""

class UserPage(webapp2.RequestHandler):
   
    def get(self):
        # Checks for active Google account session
        user = users.get_current_user()
        login_url = ''
        logout_url = ''
        activities = []
        gender = ''
        major = ''
        grade = ''
        age = ''
        primary_contact = ''
        email_address = ''
        phone_number = ''
        name = ''
        readonly = 'readonly'

        if user:
            logout_url = users.create_logout_url('/')
            fetched_user = User.query(User.email==user.email()).fetch()
            if fetched_user:
              for activity in fetched_user[0].activities :
                activities.append(activity)
              primary_contact = fetched_user[0].primary_contact
              gender = fetched_user[0].gender
              major = fetched_user[0].major
              age = fetched_user[0].age
              grade = fetched_user[0].school_year
              email_address = fetched_user[0].email
              phone_number = fetched_user[0].phone
              name = fetched_user[0].full_name

        else:
          #have else redirect to home page because you can't view your own page if you're not logged in
            login_url = users.create_login_url('/')

        templatevalues = {
          'login_url' : login_url,          
          'logout_url' : logout_url,
          'name': name,
          'activities': activities,
          'gender' : gender,
          'major' : major,
          'grade' : grade,
          'age' : age,
          'primary_contact' : primary_contact,
          'email_address' : email_address,
          'phone_number' : phone_number,
          'readonly' : readonly,
          'form_method' : 'post',
          'form_action' : 'UserPage/EditInfo',
          'submit_name' : 'Edit Information',
        }

        render_template(self, 'UserPage.html', templatevalues)

    def post(self):
        activities = []
        gender = ''
        major = ''
        grade = ''
        age = ''
        primary_contact = ''
        email_address = ''
        phone_number = ''
        name = ''
        user = self.request.get('search_param')
        fetched_user = User.query(User.full_name==user).fetch()
        if fetched_user:
          for activity in fetched_user[0].activities :
            activities.append(activity)
          primary_contact = fetched_user[0].primary_contact
          gender = fetched_user[0].gender
          major = fetched_user[0].major
          age = fetched_user[0].age
          grade = fetched_user[0].school_year
          email_address = fetched_user[0].email
          phone_number = fetched_user[0].phone
          name = fetched_user[0].full_name

          templatevalues = {
              'name': name,
              'activities': activities,
              'gender' : gender,
              'major' : major,
              'grade' : grade,
              'age' : age,
              'primary_contact' : primary_contact,
              'email_address' : email_address,
              'phone_number' : phone_number,
              'logged_in' : '',
              'readonly' : 'readonly',
              'form_method' : 'get',
              'form_action' : '/UserPage',
              'submit_name' : '',
          }

          render_template(self, 'UserPage.html', templatevalues)

        else :
          self.response.out.write("<html><body>Could not find that user</body></html>")

class EditInfo(webapp2.RequestHandler):
  def post(self):
    # Checks for active Google account session
    user = users.get_current_user()
    login_url = ''
    logout_url = ''
    activities = []
    gender = ''
    major = ''
    grade = ''
    age = ''
    primary_contact = ''
    email_address = ''
    phone_number = ''
    name = ''
    readonly = ''

    if user:
        logout_url = users.create_logout_url('/')
        fetched_user = User.query(User.email==user.email()).fetch()
        if fetched_user:
          for activity in fetched_user[0].activities :
            activities.append(activity)
          primary_contact = fetched_user[0].primary_contact
          gender = fetched_user[0].gender
          major = fetched_user[0].major
          age = fetched_user[0].age
          grade = fetched_user[0].school_year
          email_address = fetched_user[0].email
          phone_number = fetched_user[0].phone
          name = fetched_user[0].full_name

    else:
        login_url = users.create_login_url('/')

    templatevalues = {
      'login_url' : login_url,          
      'logout_url' : logout_url,
      'name': name,
      'activities': activities,
      'gender' : gender,
      'major' : major,
      'grade' : grade,
      'age' : age,
      'primary_contact' : primary_contact,
      'email_address' : email_address,
      'phone_number' : phone_number,
      'readonly' : readonly,
      'form_method' : 'post',
      'form_action' : '/UserPage/SaveInfo',
      'submit_name' : 'Save Information',
    }

    render_template(self, 'UserPage.html', templatevalues)
    self.response.out.write(CANCEL_HTML)
class SaveInfo(webapp2.RequestHandler):
  def post(self):
    user = users.get_current_user()
    fetched_user = User.query(User.email==user.email()).fetch()
    if fetched_user:
      fetched_user[0].full_name = self.request.get('full_name')
      fetched_user[0].gender = self.request.get('gender')
      fetched_user[0].age = self.request.get('age')
      fetched_user[0].major = self.request.get('major')
      fetched_user[0].phone = self.request.get('phone')
      fetched_user[0].school_year = self.request.get('grade')
      fetched_user[0].primary_contact = self.request.get('primary_contact')
      fetched_user[0].put()
    self.response.out.write("""<html><body>Updating...<script>setTimeout(function(){window.location.href="/UserPage"}, 2000)</script></body></html>
    """)


def render_template(handler, templatename, templatevalues) :
  html = template.render(templatename, templatevalues)
  handler.response.out.write(html)
            
      
application = webapp2.WSGIApplication([
    ('/UserPage', UserPage),
    ('/UserPage/EditInfo', EditInfo),
    ('/UserPage/SaveInfo', SaveInfo),
], debug=True)
     
