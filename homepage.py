import logging
from google.appengine.api import users
from google.appengine.ext import ndb
import os
import cgi  
import webapp2
from google.appengine.ext.webapp import template


class Entry(ndb.Model):
  user_email = ndb.StringProperty()
  location = ndb.StringProperty()
  start_time = ndb.StringProperty()
  end_time = ndb.StringProperty()
  is_recurring = ndb.BooleanProperty()
  date = ndb.DateProperty()
  days_recurring = ndb.IntegerProperty()

class Activity(ndb.Model):
  activity = ndb.StringProperty()
  entries = ndb.StructuredProperty(Entry, repeated=True)

class User(ndb.Model):
  last_name = ndb.StringProperty()
  first_name = ndb.StringProperty()
  email = ndb.StringProperty()
  phone = ndb.StringProperty()
  age = ndb.StringProperty()
  major = ndb.StringProperty()
  gender = ndb.StringProperty()
  school_year = ndb.StringProperty()
  primary_contact = ndb.StringProperty()
  full_name = ndb.StringProperty()
  activities = ndb.StringProperty(repeated=True)


class Week:
  pass

def main():
  logging.getLogger().setLevel(logging.DEBUG)
  webapp.util.run_wsgi_app(application)

class MainPage(webapp2.RequestHandler):

  def get(self):
  # Checks for active Google account session
    user = users.get_current_user()
    login_url = ''
    logout_url = ''
    activities = []
    logging.info('start')
    month=''
    year=''
    weeks=[]
    if user:
      fetched_user = User.query(User.email==user.email()).fetch()
      logout_url = users.create_logout_url('/')
      if fetched_user :      
        logout_url = users.create_logout_url('/')
        for activity in fetched_user[0].activities :
          activities.append(activity)    
          month = '11'
          year = '2014'
          week1 = Week()
          days = []
          vals=['', '', '', '', '', '', '']
          for i in range(26, 32):
            days.append(str(i))
            days.append('1')
            week1.days=days
            week1.vals=vals
            weeks = []
            weeks.append(week1)

          for i in range(0, 4):
            days=[]
            vals=['', '', '', '', '', '', '']
            for j in range(0, 7):
              days.append(str(7 * i + 2 + j))
              week = Week()
              week.days=days
              week.vals=vals
              weeks.append(week)  

              for activity in activities:
                fetched_activity = Activity.query(Activity.activity==activity).fetch()
                if fetched_activity:
                  fetched_entries = fetched_activity[0].entries
                  #fetched_entries = Entry.query(Entry.user_email==user.email()).fetch()
                  if fetched_entries:
                    for entry in fetched_entries :
                      if entry.user_email == user.email():
                        if entry.date:
                          if str(entry.date.month) == str(month):
                            if str(entry.date.year) == str(year):
                              for i in range(0, 5):
                                  for j in range(0, 7):
                                    if(int(weeks[i].days[j]) == int(entry.date.day)):
                                      weeks[i].vals[j] = activity + '@' + entry.location + "\n" + entry.start_time + '-' + entry.end_time    
      else :
        self.redirect('/CreateUser')    
    else:
      login_url = users.create_login_url('/')
    templatevalues = {
      'login_url' : login_url,          
      'logout_url' : logout_url,
      'month' : month,          
      'year' : year,
      'weeks': weeks,
    }
    render_template(self, 'homepage.html', templatevalues)

class adduser(webapp2.RequestHandler):
  def post(self):
    new_user = User()
    activities = []
    new_user.first_name = self.request.get('fname')
    new_user.last_name = self.request.get('lname')
    new_user.full_name = new_user.first_name + ' ' + new_user.last_name
    new_user.gender = self.request.get('gender')
    new_user.age = self.request.get('age')
    new_user.major = self.request.get('major')
    new_user.email = users.get_current_user().email()
    new_user.phone = self.request.get('phone')
      
    new_user.school_year = self.request.get('grade')
    new_user.primary_contact = self.request.get('primary_contact')
      
    for activity_name in self.request.get('activity', allow_multiple=True) :
      activities.append(activity_name)
      new_user.activities = activities
      new_user.put()
    #self.redirect('/')
    self.response.out.write("""<html><body>Your account has been successfully created! You will be redirected in a few seconds...<script>setTimeout(function(){window.location.href="/"}, 3000)</script></body></html>
        """)

class CreateUser(webapp2.RequestHandler):
  def get(self):

    #populate user fields here
    html = """
    <html>
    <body>
    Create your account:<br><br>
    <form method="post" action="adduser">
    First name: <input type="text" name="fname"><br><br>
    Last name: <input type="text" name="lname"><br><br>
    Gender: <br>
    <input type="radio" name="gender" value="Male">Male<br>
    <input type="radio" name="gender" value="Female">Female<br><br>
    Major: <input type="text" name="major"><br><br>
    Age: <input type="text" name="age"><br><br>
    School Year:    <select name="grade">
    <option value="Freshman">Freshman</option>
    <option value="Sophomore">Sophomore</option>
    <option value="Junior">Junior</option>
    <option value="Senior">Senior</option>
    </select><br><br>
    Primary Contact Method: <select name="primary_contact">
    <option value="Email">Email</option>
    <option value="Phone">Phone</option>
    </select><br><br><br><br>
    Phone Number: <input type="text" name="phone"><br><br>
    Choose a few activities that interest you:<br>
    <input type="checkbox" name="activity" value="Tennis">Tennis<br>
    <input type="checkbox" name="activity" value="Soccer">Soccer<br>
    <input type="checkbox" name="activity" value="Basketball">Basketball <br>
    <input type="checkbox" name="activity" value="Racquetball">Racquetball <br>
    <input type="checkbox" name="activity" value="Table Tennis">Table Tennis <br>
    <input type="submit" value="Submit"><br><br>
    </form>
    </body>
    </html>
    """
    self.response.out.write(html)

def render_template(handler, templatename, templatevalues) :
  html = template.render(templatename, templatevalues)
  handler.response.out.write(html)

application = webapp2.WSGIApplication([
  ('/', MainPage),
  ('/adduser', adduser),
  ('/CreateUser', CreateUser),
], debug=True)